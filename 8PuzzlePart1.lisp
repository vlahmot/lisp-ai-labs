(defun goal-state (state) 
  (equal state '(1 2 3 8 e 4 7 6 5) ))
  
(defun get-direction (move)
  (car move))
 
(defun get-state (move) 
  (car(cdr move)))
 
(defun same-state (move1 move2)
  (equal (get-state move1) (get-state move2)))
  
(defun path (moves)
  (reverse (get-path moves)))

(defun get-path (moves) 
    (cond ((null moves) nil )
	((null (get-direction (car moves))) (get-path (cdr moves)))
    (T  (cons (get-direction (car moves)) (get-path (cdr moves)))))
)
    
(defun remove-redundant (moves1 moves2)
    (set-difference moves1 moves2 :test #'same-state))
    
(defun moves (state) 
  (case (position 'e state)
    ((0) (get-moves0 (get-copies state 2)))
    ((1) (get-moves1 (get-copies state 3)))
    ((2) (get-moves2 (get-copies state 2)))
    ((3) (get-moves3 (get-copies state 3)))
    ((4) (get-moves4 (get-copies state 4)))
    ((5) (get-moves5 (get-copies state 3)))
    ((6) (get-moves6 (get-copies state 2)))
    ((7) (get-moves7 (get-copies state 3)))
    ((8) (get-moves8 (get-copies state 2)))
   ))

(defun get-copies (lst num) 
	(cond ((= 0 num) nil)
		(T (cons (copy-list lst) (get-copies lst (- num 1)))))
)
;; The get-moves#(0-8) functions refer to the position on the 8-puzzle board
;;	 0 1 2
;;   3 4 5
;;   6 7 8

(defun get-moves0 (statelst)
    (move-down (nth 0 statelst))
    (move-right (nth 1 statelst))
    (list  (list 'D (nth 0 statelst)) (list 'R (nth 1 statelst)) )
)
(defun get-moves1 (statelst)
	(move-down (nth 0 statelst))
	(move-left (nth 1 statelst))
	(move-right (nth 2 statelst))
	(list (list 'D (nth 0 statelst)) (list 'L (nth 1 statelst)) (list 'R (nth 2 statelst)))
)

(defun get-moves2 (statelst)
	(move-down (nth 0 statelst))
	(move-left (nth 1 statelst))
	(list (list 'D (nth 0 statelst)) (list 'L (nth 1 statelst)))
)

(defun get-moves3 (statelst)
	(move-down (nth 0 statelst))
	(move-up (nth 1 statelst))
	(move-right (nth 2 statelst))
	(list (list 'D (nth 0 statelst)) (list 'U (nth 1 statelst)) (list 'R (nth 2 statelst)))
)

(defun get-moves4 (statelst)
	(move-down (nth 0 statelst))
	(move-up (nth 1 statelst))
	(move-left (nth 2 statelst))
	(move-right (nth 3 statelst))
	(list (list 'D (nth 0 statelst)) (list 'U (nth 1 statelst)) (list 'L (nth 2 statelst)) (list 'R (nth 3 statelst)))
)
(defun get-moves5 (statelst)
	(move-down (nth 0 statelst))
	(move-up (nth 1 statelst))
	(move-left (nth 2 statelst))
	(list (list 'D (nth 0 statelst)) (list 'U (nth 1 statelst)) (list 'L (nth 2 statelst)))
)

(defun get-moves6 (statelst)
	(move-up (nth 0 statelst))
	(move-right (nth 1 statelst))
	(list (list 'U (nth 0 statelst))  (list 'R (nth 1 statelst)))
)

(defun get-moves7 (statelst)
	(move-up (nth 0 statelst))
	(move-left (nth 1 statelst))
	(move-right (nth 2 statelst))
	(list (list 'U (nth 0 statelst)) (list 'L (nth 1 statelst)) (list 'R (nth 2 statelst)))
)

(defun get-moves8 (statelst)
	(move-up (nth 0 statelst))
	(move-left (nth 1 statelst))
	(list (list 'U (nth 0 statelst)) (list 'L (nth 1 statelst)))
)

(defun swap (a b lst) 
  (rotatef (nth a lst) (nth b lst))
)

(defun move-left (state)
	(swap (position 'e state) (- (position 'e state) 1) state )
)

(defun move-right (state) 
	(swap (position 'e state) (+ (position 'e state) 1 ) state)
)

(defun move-up (state)
	(swap (position 'e state) (-  (position 'e state) 3) state)
)

(defun move-down (state)
	(swap (position 'e state) (+ (position 'e state) 3) state)
)
 
