
;; Below are the functions from the first lab
(defun goal-state (state) 
  (equal state '(1 2 3 8 e 4 7 6 5) ))
  
(defun get-direction (move)
  (car move))
 
(defun get-state (move) 
  (car(cdr move)))
 
(defun same-state (move1 move2)
  (equal (get-state move1) (get-state move2)))
  
(defun path (moves)
  (reverse (get-path moves)))

(defun get-path (moves) 
    (cond ((null moves) nil )
	((null (get-direction (car moves))) (get-path (cdr moves)))
    (T  (cons (get-direction (car moves)) (get-path (cdr moves)))))
)
    
(defun remove-redundant (moves1 moves2)
    (set-difference moves1 moves2 :test #'same-state))
    
(defun moves (state) 
  (case (position 'e state)
    ((0) (get-moves0 (get-copies state 2)))
    ((1) (get-moves1 (get-copies state 3)))
    ((2) (get-moves2 (get-copies state 2)))
    ((3) (get-moves3 (get-copies state 3)))
    ((4) (get-moves4 (get-copies state 4)))
    ((5) (get-moves5 (get-copies state 3)))
    ((6) (get-moves6 (get-copies state 2)))
    ((7) (get-moves7 (get-copies state 3)))
    ((8) (get-moves8 (get-copies state 2)))
   ))

(defun get-copies (lst num) 
	(cond ((= 0 num) nil)
		(T (cons (copy-list lst) (get-copies lst (- num 1)))))
)
;; The get-moves#(0-8) functions refer to the position on the 8-puzzle board
;;	 0 1 2
;;   3 4 5
;;   6 7 8

(defun get-moves0 (statelst)
    (move-down (nth 0 statelst))
    (move-right (nth 1 statelst))
    (list  (list 'D (nth 0 statelst)) (list 'R (nth 1 statelst)) )
)
(defun get-moves1 (statelst)
	(move-down (nth 0 statelst))
	(move-left (nth 1 statelst))
	(move-right (nth 2 statelst))
	(list (list 'D (nth 0 statelst)) (list 'L (nth 1 statelst)) (list 'R (nth 2 statelst)))
)

(defun get-moves2 (statelst)
	(move-down (nth 0 statelst))
	(move-left (nth 1 statelst))
	(list (list 'D (nth 0 statelst)) (list 'L (nth 1 statelst)))
)

(defun get-moves3 (statelst)
	(move-down (nth 0 statelst))
	(move-up (nth 1 statelst))
	(move-right (nth 2 statelst))
	(list (list 'D (nth 0 statelst)) (list 'U (nth 1 statelst)) (list 'R (nth 2 statelst)))
)

(defun get-moves4 (statelst)
	(move-down (nth 0 statelst))
	(move-up (nth 1 statelst))
	(move-left (nth 2 statelst))
	(move-right (nth 3 statelst))
	(list (list 'D (nth 0 statelst)) (list 'U (nth 1 statelst)) (list 'L (nth 2 statelst)) (list 'R (nth 3 statelst)))
)
(defun get-moves5 (statelst)
	(move-down (nth 0 statelst))
	(move-up (nth 1 statelst))
	(move-left (nth 2 statelst))
	(list (list 'D (nth 0 statelst)) (list 'U (nth 1 statelst)) (list 'L (nth 2 statelst)))
)

(defun get-moves6 (statelst)
	(move-up (nth 0 statelst))
	(move-right (nth 1 statelst))
	(list (list 'U (nth 0 statelst))  (list 'R (nth 1 statelst)))
)

(defun get-moves7 (statelst)
	(move-up (nth 0 statelst))
	(move-left (nth 1 statelst))
	(move-right (nth 2 statelst))
	(list (list 'U (nth 0 statelst)) (list 'L (nth 1 statelst)) (list 'R (nth 2 statelst)))
)

(defun get-moves8 (statelst)
	(move-up (nth 0 statelst))
	(move-left (nth 1 statelst))
	(list (list 'U (nth 0 statelst)) (list 'L (nth 1 statelst)))
)

(defun swap (a b lst) 
  (rotatef (nth a lst) (nth b lst))
)

(defun move-left (state)
	(swap (position 'e state) (- (position 'e state) 1) state )
)

(defun move-right (state) 
	(swap (position 'e state) (+ (position 'e state) 1 ) state)
)

(defun move-up (state)
	(swap (position 'e state) (-  (position 'e state) 3) state)
)

(defun move-down (state)
	(swap (position 'e state) (+ (position 'e state) 3) state)
)
 



;; Here are the functions for this lab
(defun make-open-init (state)
	(list(list(list nil state))))

(defun extend-path (path)
	(mapcar (lambda (x) (cons x path)) (remove-redundant (moves (get-state (first path))) path))
)

(setq test-path (first (extend-path (first (make-open-init '(2 8 3 1 6 4 7 e 5))))))

(defun search-bfs (openList)
	(cond 
		((null openList)
			nil)
		((goal-state (get-state (first (first openList))))
			(path (first openList)))
		(T (search-bfs (append (rest openList) (extend-path (first openList)))))
	)
)
(defun search-dfs-fd (openList depthBound)
	(cond 
		((null openList)
			 nil)
		((goal-state (get-state (first (first openList))))
			(path (first openList)))
		((>(-(length (first openList)) 1) depthBound ) 
			(search-dfs-fd (rest openList) depthBound))
		(T (search-dfs-fd (append  (extend-path (first openList)) (rest openList)) depthBound))
	)
)

;;Note this will actually run the "winning" dfs twice, once to check for null and then once to return the answer
(defun search-id (openList &optional (depthBound 0 ) )
	(cond 
		((null (search-dfs-fd openList depthBound))
			(search-id openList (+ 1 depthBound)))
		(T (search-dfs-fd openList depthBound))
	)
)

(defun sss (initialState &key (type 'BFS) (depth 7))
	(cond
		((goal-state initialState) nil)

		((eq type 'DFS)
			(search-dfs-fd (make-open-init initialState) depth)
		)

		((eq type 'ID)
			(search-id (make-open-init initialState))
		)

		(T
			(search-bfs (make-open-init initialState))
		)
	)
)

