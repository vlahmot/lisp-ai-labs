(load "C:/Users/Admin/Desktop/Projects/ai/solnA1.lisp")
(load "C:/Users/Admin/Desktop/Projects/ai/solnA2.lisp")

(defun out-of-place (state)
	(out-of-placeH state '( 1 2 3 8 e 4 7 6 5))	
)

(defun out-of-placeH (lst1 lst2) 
	(cond 
		((null lst1) 0)
		(   (equal (car lst1) 'e)  
			(out-of-placeH (rest lst1) (rest lst2) )
		)
		((equal (car lst1) (car lst2)) 
			(out-of-placeH (rest lst1) (rest lst2) )
		)
		(t (+ 1 (out-of-placeH (rest lst1) (rest lst2) )) )
	)
)

(defun out-of-place-f (path)
	(+ (out-of-place (get-state (first path)))  (- (length path)  1)    )

)

(defun manhattan (state)
	(manhattanH state 1)
)

(defun manhattanH (state index) 

	(cond 
		((null state) 0)
		(t ( + (get-offset (first state) index) (manhattanH (rest state) (+ index 1))))
	)
)
(defun get-offset (value index)
	(cond 
		((equal value 'e) 0)
	(t (calc-manh  (get-goal-x-y value) (get-x-y index) ))
	)
)
(defun get-goal-x-y (value)
	(cond
		((equal value 1) '(1 1))
		((equal value 2) '(2 1))
		((equal value 3) '(3 1))
		((equal value 4) '(3 2))
		((equal value 5) '(3 3))
		((equal value 6) '(2 3))
		((equal value 7) '(1 3))
		(t '(1 2))

	)		
)

;1 based index
(defun get-x-y (index)
	(cond
		((= index 1) '(1 1))
		((= index 2) '(2 1))
		((= index 3) '(3 1))
		((= index 4) '(1 2))
		((= index 5) '(2 2))
		((= index 6) '(3 2))
		((= index 7) '(1 3))
		((= index 8) '(2 3))
		(t '(3 3))
	)	
)

(defun calc-manh (coord1 coord2)
	(+ (abs (-(nth 0 coord1) (nth 0 coord2))) (abs (- (nth 1 coord1) (nth 1 coord2))))
)
(defun manhattan-f (path)
	(+ (manhattan (get-state (first path)))  (- (length path)  1)    )

)

(defun better (func) 
	(lambda (path1 path2)
		(cond 
		((= (funcall func path1) (funcall func path2)) t)
		(t (< (funcall func path1) (funcall func path2)))
		)
	)
)
(defun search-a* (openList heuristic)
	(cond 
		((null openList)
			nil)
		((goal-state (get-state (first (first openList))))
			(path (first openList)))
		(T (search-a* (sort (append (rest openList) (extend-path (first openList))) (better heuristic)) heuristic))
	)
)
(defun sss (initialState &key (type 'BFS) (depth 7) (F #'out-of-place-f))
	(cond
		((goal-state initialState) nil)
		((eq type 'a*)
			(search-a* (make-open-init initialState) F)
		)

		((eq type 'DFS)
			(search-dfs-fd (make-open-init initialState) depth)
		)

		((eq type 'ID)
			(search-id (make-open-init initialState))
		)

		(T
			(search-bfs (make-open-init initialState))
		)
	)
)

; 1) DFS with a depth of 10 and A* with manhattan distance used got me .015 seconds
; 2) Manhattan .03 compared to .14
; 3) yes, a depth of 10 just barely beats a*