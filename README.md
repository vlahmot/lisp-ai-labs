This is a collection of lab assignments that I have completed
for my Artificial Intelligence course. The labs consists of an introduction
to Common Lisp, state space search, and solving the 8-puzzle game.
The search methods include depth first, breadth first, and A*. 
