(defun my-rotate (x)
	(if (null x) nil
	(nconc (cdr x) (list (car x)))))

(defun my-rotate-n (n lis) 
	(cond 
		((= n 0) lis)
		(T (my-rotate-n (- n 1) (my-rotate lis)) )))

(defun first_sat ( lis1 lis2 foo )
	(cond 
		( (or (null lis1) (null lis2) (null foo) ) nil)
		( (funcall foo (first lis1) (first lis2))
			(list (first lis1) (first lis2)))
		(t (first_sat (rest lis1) (rest lis2) foo))))

(defun my-remove (ato lis)
	(cond 
		((null lis) nil)
		((eq ato (car lis)) (my-remove ato (cdr lis)))
		(T (cons 
				(if (atom (car lis)) 
					(car lis) 
					(my-remove ato (car lis)))
			(my-remove ato (cdr lis))))
	)
)

(defun palindromep (lis) 
	(cond
		((null lis))
		((equal (first lis) (nth ((-(list-length lis) 1))) lis)) (palindromep (cdr (butlast lis))))
		(t nil)
	)
)
		